package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class GatherDeviceUpgradeOffersTests {

	@Test
	public void testResourceInitialisation() {
		GatherDeviceUpgradeOffersResource resource = new GatherDeviceUpgradeOffersResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetDeviceUpgradesNoSecurity() {
		GatherDeviceUpgradeOffersResource resource = new GatherDeviceUpgradeOffersResource();
		resource.setSecurityContext(null);

		Response response = resource.getdeviceUpgrades();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetDeviceUpgradesNotAuthorised() {
		GatherDeviceUpgradeOffersResource resource = new GatherDeviceUpgradeOffersResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getdeviceUpgrades();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetDeviceUpgradesAuthorised() {
		GatherDeviceUpgradeOffersResource resource = new GatherDeviceUpgradeOffersResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getdeviceUpgrades();
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}