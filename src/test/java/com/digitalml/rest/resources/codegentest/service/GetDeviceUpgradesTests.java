package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.GatherDeviceUpgradeOffers.GatherDeviceUpgradeOffersServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GatherDeviceUpgradeOffersService.GetDeviceUpgradesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GatherDeviceUpgradeOffersService.GetDeviceUpgradesReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetDeviceUpgradesTests {

	@Test
	public void testOperationGetDeviceUpgradesBasicMapping()  {
		GatherDeviceUpgradeOffersServiceDefaultImpl serviceDefaultImpl = new GatherDeviceUpgradeOffersServiceDefaultImpl();
		GetDeviceUpgradesInputParametersDTO inputs = new GetDeviceUpgradesInputParametersDTO();
		GetDeviceUpgradesReturnDTO returnValue = serviceDefaultImpl.getdeviceUpgrades(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}